# Use an official ubuntu runtime as a parent image
FROM ubuntu
# Set the working directory to /app
WORKDIR /app
# Copy the current directory contents into the container
COPY . /var/www/html
# Install any needed packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt -y update
RUN apt -y install apache2

RUN apt -y install php libapache2-mod-php
RUN apt -y install nano
RUN echo "ServerName localhost" >>/etc/apache2/apache2.conf
# Make port 80 available to the world outside this container
EXPOSE 80

CMD apachectl -D FOREGROUND